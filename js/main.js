$(document).ready(function() {

    //hero carousel
    $('.main-carousel').slick({
        dots: true,
        fade: true,
        cssEase: 'linear',
        centerMode: true,
        arrows: false,
    });


    //case studies carousel
    $('.case-carousel').slick({
        arrows: true,
        dots: false,
        slidesToShow: 2,
        // centerPadding: '60px',
        responsive: [
            {
              breakpoint: 1024,
              settings: {
                slidesToShow: 2,
                slidesToScroll: 1,
                infinite: true,
                dots: true,
                centerPadding: "50px",
            }
        },
        {
            breakpoint: 700,
            settings: {
                slidesToShow: 1,
                slidesToScroll: 1,
                centerPadding: "0",
            }
        },
        {
            breakpoint: 480,
            settings: {
                slidesToShow: 1,
                slidesToScroll: 1,
                centerPadding: "0",
              }
            }
            // You can unslick at a given breakpoint now by adding:
            // settings: "unslick"
            // instead of a settings object
        ],
        prevArrow:"<img class='a-left control-c prev slick-prev' src='../icons/Icon feather-chevron-left.svg'>",
        nextArrow:"<img class='a-right control-c next slick-next' src='../icons/Icon feather-chevron-right.svg'>",
    });


    //menu
    var menuBar = $('.main-nav');
    var hamBurg = $('#hamburger');

    // $(window).on('resize', function() {
    //     menuBar.hide();
    // });

    if(window.innerWidth < 767) {
        menuBar.hide();
        
    }
    if(menuBar.length && window.innerWidth < 767) {
        $('.main-nav__link').on('click', function() {
            menuBar.hide();
        });
    }

    hamBurg.on('click', function() {
        $(this).toggleClass('change_opacity');
        if(window.innerWidth < 767) {
            menuBar.slideToggle(300);
        }
    });
    
    
    //menu scroll link 
    var scrollLink = $('.main-nav__link');
    
    //smooth scrolling
    scrollLink.click(function(e) {
        e.preventDefault();
        $('body, html').animate({
            scrollTop: $(this.hash).offset().top
        }, 50);
    });

    //active link switching
    $(window).scroll(function() {
        var scrollbarLocation = $(this).scrollTop();

        scrollLink.each(function() {
            var sectionOffset = $(this.hash);
            if(sectionOffset.length) {
                sectionOffset = sectionOffset.offset().top;
            }
                

            if(sectionOffset <= scrollbarLocation) {
                $(this).addClass('active');
                $(this).parent().siblings().children('a').removeClass('active');
            }
        });
    });

    var target = $('.process-controls__single:first a')[0];
    target.click();

    $(window).on('scroll', function() {
        $('.arrow_down').hide();
    })

});