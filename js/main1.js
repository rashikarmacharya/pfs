(function() {



    var tabMenus = document.querySelectorAll('.menu_item_link');
    var tabContents = document.querySelectorAll('.tab_content');


    for(let i=0; i<tabMenus.length; i++) {
        tabMenus[i].addEventListener('click', function(e){
            e.preventDefault();
            console.log('link is clicked');
            this.parentNode.classList.add('acitve');

            for (var i = 0; i < tabMenus.length; i++) {
                tabMenus[i].className = 'menu_item_link';
            }

            this.className = 'menu_item_link active';

            for(var i = 0; i < tabContents.length; i++ ) {
                tabContents[i].className = 'tab_content';
            }

            document.getElementById(this.dataset.id).className = 'tab_content active';
        });
    }


    //hide shoe menu 
    var menuControl = document.querySelector('.header_main');
    var prevScrollpos = window.pageYOffset;
    window.onscroll = function() {
    var currentScrollPos = window.pageYOffset;
    if (prevScrollpos > currentScrollPos) {
        menuControl.style.top = "0";
        menuControl.style.transition = "top 0.3s ease";
    } else {
        menuControl.style.top = "-100%";
        menuControl.style.transition = "top 0.3s ease";
    }
    prevScrollpos = currentScrollPos;
    }
})();

