var gulp = require('gulp');
var sass = require('gulp-sass');
var uglifycss = require('gulp-uglifycss');
var autoprefixer = require('gulp-autoprefixer');

gulp.task('sass', function() {
    return gulp.src('./sass/*.scss')
    .pipe(sass().on('error', sass.logError))
    .pipe(gulp.dest('./css'));
}); 

gulp.task('autoprefix', function() {
    return gulp.src('./dist/*.css')
    .pipe(autoprefixer({
      browsers: ['last 2 versions']
    }))
    .pipe(gulp.dest('./dist/'))
});

gulp.task('css', function () {
    gulp.src('./css/*.css')
      .pipe(uglifycss({
        "uglyComments": true
      }))
      .pipe(gulp.dest('./dist/'));
});


// gulp.task('run', ['sass', 'css']);

// gult.task('watch', function(){
//     gulp.watch('./sass/*.scss', ['sass']);
//     // gulp.watch('./css/*.css', ['css']);
// });

// gult.task('default', ['run', 'watch']);